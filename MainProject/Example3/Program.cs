﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example3
{
    class Program
    {
        static long Factorial(long n)
        {
            long res = 1;
            do
            {
                res = res * n;
            }
            while (--n > 0);
            return res;
        }
        static void Main()
        {
            long res1 = 0, res2 = 0;
            long n1 = 3, n2 = 4;
            Thread t1 = new Thread(() =>
              {
                  res1 = Factorial(n1);
              }); 
              Thread t2 = new Thread(() => { res2 = Factorial(n2); });
              // Запускаем потоки 
              t1.Start();  t2.Start(); 
              // Ожидаем завершения потоков 
              t1.Join();  t2.Join(); 
            Console.WriteLine("Факториал {0} равен {1}", n1, res1); 
            Console.WriteLine("Факториал {0} равен {1}", n2, res2); 
            Console.ReadLine();
        }
    } 
}
