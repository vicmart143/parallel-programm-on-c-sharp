﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example7
{
    class Program
    {
        private static object _lock = new object();
        static public string data =  "CCC";
        static void DoSomeWork1()
        {
            lock(data)
            {
                data = "AAAA";
                Console.WriteLine("Поток #1 изменил данные: {0}", data);   
            }
        }
        static void DoSomeWork2()
        {
            data = "BBBB";
            Console.WriteLine("Поток: {0}, Data: {1}", Thread.CurrentThread.Name, data);
        }
        static void Main(string[] args)
        {

            Thread thr1 = new Thread(DoSomeWork1);
            Thread thr2 = new Thread(DoSomeWork2);
            thr1.Name = "Первый";
            thr2.Name = "Второй";
            thr1.Start();
            thr2.Start();

            Console.ReadLine();
        }

    }
}


