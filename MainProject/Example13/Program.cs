﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Example13
{
    class Program
    {
        static void Main(string[] args)
        {
            Task tParent = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Старт родительской задачи");
                Task t1 = Task.Factory.StartNew(() =>
                    Console.WriteLine("Недочерняя задача"));
                Task t2 = Task.Factory.StartNew(() =>
                    Console.WriteLine("Дочерняя задача"),
                    TaskCreationOptions.AttachedToParent);
                Console.WriteLine("Родительская задача завершена");
            });
            tParent.Wait();
            Console.WriteLine("Родительская задача на самом деле завершена");
            Console.Read();
        }
    }
}
