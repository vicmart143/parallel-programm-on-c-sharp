﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example11
{
    class Program
    {
        static void HelloWorld()
        {
            Console.WriteLine("Hello, world!");
        }
        static void Main()
        {
            // Используем обычный метод  
            Task t1 = new Task(HelloWorld);
            // Используем делегат Action  
            Task t2 = new Task(new Action(HelloWorld));
            // Используем безымянный делегат 
            Task t3 = new Task(delegate
            {
                HelloWorld();
            });
            // Используем лямбда-выражение  
            Task t4 = new Task(() => HelloWorld());
            // Используем лямбда-выражение  
            Task t5 = new Task(() =>
            {
                HelloWorld();
            });
            Task t6 = new Task(() =>
            {
                Console.WriteLine("Hello, world!");
            });

            // Запускаем задачи 
            t1.Start(); t2.Start(); t3.Start();
            t4.Start(); t5.Start(); t6.Start();
            // Дожидаемся завершения задач 
            Task.WaitAll(t1, t2, t3, t4, t5, t6);
            Console.Read();
        }
    }
}
