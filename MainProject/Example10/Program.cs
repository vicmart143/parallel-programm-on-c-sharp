﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Collections.Concurrent;
namespace Example10
{
    class Program
    {
        private static void Main()
        {
            var bag = new ConcurrentBag<int>();
            for (int i = 0; i < 10; i++)
                bag.Add(i);

            foreach (int k in bag)
            {
                bag.Add(k);
                Console.Write(bag.Count + " ");
            }
            Console.Read();
        }
    }
}
