﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Example12
{
    class Program
    {
        static string programName;
        static void ShowTaskInfo(object taskName)
        {
            Console.WriteLine("Имя задачи: {0}, ID задачи: {1}, Поток {2}, Program name: {3}",
                taskName, Task.CurrentId, Thread.CurrentThread, programName);
        }
        static void Main()
        {
            programName = "Работа с данными";
 //Если рабочий элемент это метод класса, то работать можно с переменными этого класса.
            Task t1 = Task.Factory.StartNew(new Action<object>(ShowTaskInfo),
                "Первая задача");
            Task t2 = Task.Factory.StartNew(o => ShowTaskInfo(o),
                "Вторая задача");
            string t3Name = "Третья задача";
// Если рабочим элементом является лямбда-выражение, 
// то работать можно со всеми локальными переменными метода, порождающего задачу.
            Task t3 = Task.Factory.StartNew(() =>
                    ShowTaskInfo(t3Name));
// Свойство Result содержит результат задачи.Обращение к свойству блокирует поток до завершения задачи.
            long lNumber = 123456789;
            Task<double> SqrtTask = Task.Factory.StartNew((obj) =>
            {
                return Math.Sqrt((long)obj);
            }, lNumber);
            // Дожидаемся завершения вычислений без явной блокировки 
            double sqrtNumber = SqrtTask.Result;
            Console.WriteLine("Результат: {0}", sqrtNumber);
            Console.Read();
        }
    }
}
