﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Example17
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> words = new List<string> {"Один",
            "Два", "Три", "Четыре", "Пять",
            "Шесть", "Семь", "Восемь", "Девять", "Десять" };
            Parallel.ForEach(words, s => Console.WriteLine(s));
            Console.Read();
        }
    }
}
