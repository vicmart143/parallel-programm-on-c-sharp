﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Example18
{
    class Program
    {
        static void Main(string[] args)
        {
            var bag1 = new ConcurrentBag<int>();
            Parallel.For(0, 1000, (int i, ParallelLoopState pState) =>
            {
                if (i == 50)
                    pState.Break();
                else
                {
                    Thread.Sleep(10);
                    bag1.Add(i);
                }
            });

            var bag2 = new ConcurrentBag<int>();
            Parallel.For(0, 10000, (i, state) =>
            {
                if (i == 50)
                    state.Stop();
                else
                {
                    Thread.Sleep(10);
                    bag2.Add(i);
                }

            });
            Console.WriteLine("Остановка 50, Меньший: {0}, Больший: {1}",
              bag1.Where(i => i < 50).Count(),
              bag1.Where(i => i > 50).Count());

            Console.WriteLine("Остановка 50, Меньший: {0}, Больший: {1}",
              bag2.Where(i => i < 50).Count(),
              bag2.Where(i => i > 50).Count());
            Console.Read();
        }
    }
}
