﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Example16
{
    class Program
    {
        static int Sq(int x)
        {
            return x * x;
        }
        static void Main(string[] args)
        {
            int[] data = new int[10];
            int[] results = new int[10];
            // Последовательный цикл 
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = i;
                Console.WriteLine("Значение: {0}", data[i]);
            }
            // Параллельный цикл 
            Parallel.For(0, data.Length, i =>
               {
                   results[i] = Sq(data[i]);
                   Console.WriteLine("Квадрат: {0}", results[i]);
               });
            Console.WriteLine("Сумма: {0}", results.Sum());
            Console.Read();
        }
    }
}
