﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example5
{
    class Program
    {
        static void Func1(object o)
        {
            var ev = (ManualResetEvent)o;
            Console.WriteLine("Func1: Working..");
            ev.Set();
        }
        static void Main(string[] args)
        {
            ManualResetEvent ev = new ManualResetEvent(false);
            ThreadPool.QueueUserWorkItem(Func1, ev);
            ev.WaitOne();
            for (int i = 0; i < 10; i++)
            {
                ThreadPool.QueueUserWorkItem((object o) =>
                {
                    Console.WriteLine("i: {0}, ThreadId: {1},IsPoolThread: {2}",
                        i,
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.IsThreadPoolThread);
                });
                Thread.Sleep(10);
            }
            Console.Read();
        }

    }
}
