﻿using System;
using System.Threading;


namespace Example1
{
    class Program
    {
        static void LocalWorkItem()
        {
            Console.WriteLine("Статический метод LocalWorkItem()");
        }
        static void Main()
        {
            Thread thr1 = new Thread(LocalWorkItem);
            thr1.Start();

            Thread thr2 = new Thread(() =>
            {
                Console.WriteLine("Лямбда-выражение"); 
              });
            thr2.Start();

            ThreadClass thrClass = new ThreadClass("Медод класса ThreadClass");
            Thread thr3 = new Thread(thrClass.Run);
            thr3.Start();


            Thread thr4 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                    Console.Write("A");
            });
            Thread thr5 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                    Console.Write("B");
            });
            Thread thr6 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                    Console.Write("C");
            });
            thr4.Start();
            thr5.Start();
            thr4.Join();
            thr5.Join();
            thr6.Start();

            Console.Read();


        }
    }
    class ThreadClass
    {
        private string Message;
        public ThreadClass(string sMessage)
        {
            Message = sMessage;
        }
        public void Run()
        {
            Console.WriteLine(Message);
        }
    }
}

