﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Example8
{
    class Program
    {
        class SemaphoreSlimClass
        {
            private static SemaphoreSlim sem;
            private static void Worker(object num)
            {
                // Ждем сигнала от управляющего 
                sem.Wait();
                // Начинаем работу 
                Console.WriteLine("Рабочий {0} начал", (int)num + 1);
            }
            private static void Main()
            {
                // Максимальная емкость семафора: 5 
                // Начальное состояние: 0 (все блокируются) 
                sem = new SemaphoreSlim(0, 5);
                Thread[] workers = new Thread[10];
                for (int i = 0; i < workers.Length; i++)
                {
                    workers[i] = new Thread(Worker);
                    workers[i].Start(i);
                }
                Thread.Sleep(300);
                Console.WriteLine("Разрешаем работу трем рабочим");
                sem.Release(3);
                Thread.Sleep(200);
                Console.WriteLine("Разрешаем работу еще двум рабочим");
                sem.Release(2);

            }
        }
    }
}
