﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Runtime;


namespace Example9
{
    class Program
    {
        private static void Main() {
            for (var j = 0; j < 20; j++)
            {
                // Создаем обычный словарь 
                var dic = new Dictionary<string, int>();
                // Параллельно обновляем значение элемента с ключом "one" 
                Parallel.For(0, 100000, i =>
                {
                    lock (dic)
                    {
                        if (dic.ContainsKey("one"))
                            dic["one"]++;
                        else
                            dic.Add("one", 1);
                    }
                });
                Console.WriteLine("Element 'one': {0}", dic["one"]);
            }
            Console.Read(); 
        }
    }
}
