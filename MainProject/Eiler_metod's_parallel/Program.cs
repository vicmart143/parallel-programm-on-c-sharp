﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;


namespace Eiler_metod_s
{

    class Eiler
    {
        static int N = 1000;
        public double dt = 0.01;
        double[] G = new double[N * N];
        double[] Vold = new double[N];
        public double[] Vnew = new double[N];
        public double[] m = new double[N];
        public double[] h = new double[N];
        public double[] n = new double[N];
        public double[] I_ext = new double[N];
        double[] rold = new double[N];
        public double[] rnew = new double[N];
        public const double V_na = -115.0, V_k = 12.0, V_l = -10.613, g_na = 120.0, g_k = 36.0, g_l = 0.3,
                a = 0.5, b = 0.1, E_syn = -10.0, a_ex = 2, b_ex = 1, E_ex = 70.0;



        public Eiler()
        {

            for (int i = 0; i < N; i++)
            {
                rold[i] = m[i] = n[i] = h[i] = 0.0;
                for (int j = 0; j < N; j++)
                {
                    G[i * N + j] = 0.001;
                    Vold[i] = -9.0;
                }

            }

        }


        public void Solve(int Time, Random rnd)
        {
            int  count;

            // цикл по времени
            for (count = 0; count < Time; count++)
            {
                GeniExt(rnd);
                // цикл по нейронам
                Parallel.For(0, N, ind => Neurons(ind));
                for (var i = 0; i < N; i++)
                {
                    Vold[i] = Vnew[i];
                    rold[i] = rnew[i];
                }

            }
        }
        public void GeniExt(Random rnd)
        {
            for (var i = 0; i < N; i++)
                I_ext[i] = -10.0 - rnd.NextDouble();
        }

        public void Neurons(int i)
        {
            Vnew[i] = Vold[i] + F_v(i, G, Vold[i], rold, m[i], h[i], n[i], I_ext[i], i) * dt;  // на шаге 330 создает NaN
            m[i] = m[i] + F_m(Vold[i], m[i]) * dt;
            h[i] = h[i] + F_h(Vold[i], h[i]) * dt;
            n[i] = n[i] + F_n(Vold[i], n[i]) * dt;
            rnew[i] = rold[i] + F_r(Vold[i], rold[i]) * dt;

        }

    public double F_v(int num, double[] g, double V, double[] r, double m, double h, double n, double I_ext, int index)
        {
            double I_k = g_k * n * n * n * n * (V + V_k);
            double I_na = g_na * m * m * m * h * (V + V_na);
            double I_leak = g_l * (V + V_l);
            double I_syn = 0.0;
            for (int j = 0; j < N; j++)
            {
                I_syn = I_syn - r[j] * g[index * N + j] * (E_syn - V);
            }

            double res = -(I_k + I_na + I_na + I_leak + I_syn + I_ext);
            return res;
        }

        public double F_m(double V, double m)
        {
            double alpha = (0.1 * (-V + 25.0)) / (Math.Exp((-V + 25.0) / 10.0) - 1.0);
            double beta = 4.0 * Math.Exp(-V / 18.0);
            double res = alpha * (1 - m) - beta * m;
            return res;
        }

        public double F_h(double V, double h)
        {
            double alpha = 0.07 * Math.Exp(-V / 20.0);
            double beta = 1.0 / (Math.Exp((-V + 30.0) / 10.0) + 1.0);
            double res = alpha * (1 - h) - beta * h;
            return res;
        }

        public double F_n(double V, double n)
        {
            double alpha = (0.01 * (-V + 10.0)) / (Math.Exp((-V + 10.0) / 10.0) - 1.0);
            double beta = 0.125 * Math.Exp(-V / 80.0);
            double res = alpha * (1 - n) - beta * n;
            return res;
        }

        public double F_r(double V, double r)
        {
            double T = 1.0 / (1.0 + Math.Exp(-(V - 20.0) / 2.0));
            double res = a * T * (1 - r) - b * r;
            return res;
        }

        public double F_gi(double r_pre, double r_post, double g)
        {
            return 0.0;
        }

        public void Print()
        {
            FileStream f = new FileStream("D:\\test.csv", FileMode.Create);
            StreamWriter writer = new StreamWriter(f);
            writer.WriteLine("index;Vold;m;h;n;rold");
            for (int i = 0; i < N; i++)
            {
                writer.WriteLine("{0};{1};{2};{3};{4};{5}", i, Vold[i], m[i], h[i], n[i], rold[i]);
            }
            writer.Close();
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
                int seed = 1;
                Random Rnd = new Random(seed);
                Eiler eiler = new Eiler();
                DateTime t1 = DateTime.Now;
                eiler.Solve(20000, Rnd);
                DateTime t2 = DateTime.Now;
                eiler.Print();
                Console.WriteLine("Итерация {1}, время выполнения = {0}", t2 - t1, 1);
            Console.Read();
        }







    }
}
