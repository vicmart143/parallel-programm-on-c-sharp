﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example6
{
    class Program
    {
        static bool b;
        static double res;
        static void SomeWork()
        {
            for (int i = 0; i < 100000; i++)
                for (int j = 0; j < 20; j++)
                    res += Math.Pow(i, 1.33);
            b = true;
        }
        static void Main()
        {
            Thread thr1 = new Thread(SomeWork);
            thr1.Start();
            // Активное ожидание в цикле 
            while (!b) ;
            Console.WriteLine("Result = " + res);

            res = 0;
            Thread thr2 = new Thread(SomeWork);
            thr2.Start();
            // Ожидание с выгружением контекста 
            thr2.Join();
            Console.Read();

        }
    }
}
