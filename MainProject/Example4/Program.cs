﻿using System;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example4
{
    class PriorityTesting
    {
        static long[] counts;
        static bool finish;
        static void ThreadFunc(object iThread)
        {
            while (true)
            {
                if (finish)
                    break;
                counts[(int)iThread]++;
            }
        }
        static void Main()
        {
            counts = new long[5];
            Thread[] t = new Thread[5];
            for (int i = 0; i < t.Length; i++)
            {
                t[i] = new Thread(ThreadFunc);
                t[i].Priority = (ThreadPriority)i;
            }
            // Запускаем потоки 
            for (int i = 0; i < t.Length; i++)
                t[i].Start(i);

            // Даём потокам возможность поработать 3 c 
            Thread.Sleep(3000);

            // Сигнал о завершении 
            finish = true;

            // Ожидаем завершения всех потоков 
            for (int i = 0; i < t.Length; i++)
                t[i].Join();
            // Вывод результатов 
            for (int i = 0; i < t.Length; i++)
                Console.WriteLine("Поток с приоритетом {0}, счетчик: {1}", (ThreadPriority)i, counts[i]);
            Console.Read();
        }
    }
}
